import { Fragment, useState, useEffect, useContext } from 'react';

import ProductCardUser from './../components/ProductCardUser';
import ProductCardGuest from './../components/ProductCardGuest';

import AppHeader from './../components/AppHeader';

import UserContext from './../UserContext';

export default function Home () {
	
	const { user } = useContext(UserContext);
	const [ products, setProducts ] = useState([]);

	useEffect(() => {
		const fetchProducts = () => {
			fetch('https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/active-products').then(response => response.json()).then(result => {
				//console.log(result);
				setProducts(result);
			})
		}

		fetchProducts();
	}, [])

	return (

		<Fragment>
			<AppHeader />
			<div className="d-flex flex-wrap justify-content-center">
			{
				(user.id !== null) ?
				<ProductCardUser productProperty={products}  />
				:
				<ProductCardGuest productProperty={products}/>
			}
			</div>
		</Fragment>
	
	)
}