import { useEffect, useState, useContext, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import UserContext from './../UserContext'

export default function MyOrders () {

	const { user } = useContext(UserContext);

	const [ orders, setOrders ] = useState([]);
	const [ orderData, setOrderData ] = useState([]);
	const [ orderLength, setOrderLength ] = useState(0);

	useEffect(() => {
		

		const fetchOrders = () => {
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/fulfilled-orders`, {
				method: "GET",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then(response => {
				setOrderLength(response.length)
				setOrders(response);
			})
		}	

		fetchOrders();


		function ConvertMonth (Month) {
			let MonthConverted;
				if (Month === "01") {
				MonthConverted = "Jan"
				return MonthConverted
			} else if (Month === "02"){
				MonthConverted = "Feb"
				return MonthConverted
			} else if (Month === "03"){
				MonthConverted = "Mar"
				return MonthConverted
			} else if (Month === "04") {
				MonthConverted = "Apr"
				return MonthConverted
			} else if (Month === "05") {
				MonthConverted = "May"
				return MonthConverted
			} else if (Month === "06") {
				MonthConverted = "Jun"
				return MonthConverted
			} else if (Month === "07") {
				MonthConverted = "Jul"
				return MonthConverted
			} else if (Month === "08") {
				MonthConverted = "Aug"
				return MonthConverted
			} else if (Month === "09") {
				MonthConverted = "Sept"
				return MonthConverted
			} else if (Month === "10") {
				MonthConverted = "Oct"
				return MonthConverted
			} else if (Month === "11") {
				MonthConverted = "Nov"
				return MonthConverted
			} else {
				MonthConverted = "Dec"
				return MonthConverted
			};
		}

		function ConvertedDate (wholeDate) {
			let Year = wholeDate.slice(0,4);
			let Month = wholeDate.slice(5,7);
			let Day = wholeDate.slice(8,10);

			let Hour = wholeDate.slice(11,13);
			let Minute = wholeDate.slice(14,16);
			
			let wholeStatement;

			wholeStatement = `${ConvertMonth(Month)} ${Day}, ${Year} at ${Hour}:${Minute}`;

			return wholeStatement
		}

		const orderInfo = orders.map((order) => {
			const { purchasedOn, totalAmount, orderedProducts, _id } = order

			return (
			<tr>
				<td>
				{
					orderedProducts.map((product) => {
				const { productName, productQuantity, subtotal } = product
				return (
					<tr>{`Product Name: ${productName} Qty: ${productQuantity} Subtotal: Php${subtotal}`}</tr>
					)
				})
				}
				</td>
					<td>{_id}</td>
					<td>{ConvertedDate(purchasedOn)}</td>
					<td>Php {totalAmount}</td>
					<td>Checked Out</td>
			</tr>
			)
		})

		setOrderData(orderInfo)
		
	},[orders])

	return (
		<Fragment>
			{
				(user.id !== null) ?
					<div>
						<div className="bg-dark p-2">
							<h1 className="display-4 fw-bolder p-2 text-white d-flex justify-content-center">My Orders</h1>
						</div>
						{
							(orderLength !== 0) ?
							<div className="p-5">	
								<Table striped bordered hover>
									<thead>
										<tr>
											<th>Ordered Products</th>
											<th>Ref. No.</th>
											<th>Date Purchased</th>
											<th>Total</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										{orderData}
									</tbody>
								</Table>
							</div>	
							:
							<div className="d-flex justify-content-center m-5 p-5">
								<h1>No Orders Yet</h1>
							</div>
						}
					</div>
				:
					<Redirect to="/" />
			}
		</Fragment>
	)
}


