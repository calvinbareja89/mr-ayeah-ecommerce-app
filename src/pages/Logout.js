import { Redirect } from 'react-router-dom';
import UserContext from './../UserContext';
import { useContext } from 'react';

export default function Logout () {
	
	const { setUser } = useContext(UserContext);

	setUser({
			id: null,
			email: null
		})

	localStorage.clear()

	return (

		<Redirect to="/login" />
	)
}