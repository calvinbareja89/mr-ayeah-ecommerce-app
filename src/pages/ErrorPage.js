import { Container, Col, Row } from 'react-bootstrap';

export default function ErrorPage () {
	return (
		<Container className="jumbotron m-5 p-5">
			<Col>
				<Row>
					<h1>Error 404: Page Not Found!</h1>
				</Row>
				<Row>
					<p>The page cannot be found. You may go back to the <a href="/">Home Page</a></p>
				</Row>
			</Col>
		</Container>
	)
}