//Bootstrap Component Modules
import { Card, Button, Modal, Form } from 'react-bootstrap';
import { Fragment, useEffect, useState} from 'react';

import Swal from 'sweetalert2';

export default function ProductCardAdmin (props) {
	//console.log(props)
	const { productProperty, fetchProducts} = props
	const [productCard, setProductCard] = useState([]);

	

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	//Update
	const [showEdit, setShowEdit] = useState(false)
	const openEdit = () => setShowEdit(true)
	const closeEdit = () => {
		setProductId("");
		setPreviousName("");
		setPreviousDescription("");
		setPreviousPrice(0);
		setShowEdit(false)
	}

	//Disable
	const [showDisable, setShowDisable] = useState(false)
	const openDisable = () => setShowDisable(true)
	const closeDisable = () => {
		setProductId("")
		setShowDisable(false)
	}

	//Enable
	const [showEnable, setShowEnable] = useState(false)
	const openEnable = () => setShowEnable(true)
	const closeEnable = () => {
		setProductId("");
		setShowEnable(false)
	}

	//Delete

	//Enable
	const [showDelete, setShowDelete] = useState(false)
	const openDelete = () => setShowDelete(true)
	const closeDelete = () => {
		setProductId("");
		setShowDelete(false);
	}

	const [ productID, setProductId] = useState("");

	const [ previousName, setPreviousName] = useState("");
	const [ previousDescription, setPreviousDescription] = useState("");
	const [ previousPrice, setPreviousPrice ] = useState(0);

	function updateProduct (productId) {	
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/${productId}/update-product`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
 			},
 			body: JSON.stringify({
 				productName: name,
 				productDescription: description,
 				productPrice: price
 			})
		}).then(response => response.json()).then(response => {
			//console.log(response);
			if (response) {
				Swal.fire({
					title: "Update Product",
					icon: "success",
					text: "Item Enabled!"
				})
				fetchProducts()
				setProductId("");
				setPreviousName("");
				setPreviousDescription("");
				setPreviousPrice(0);
				closeEdit()
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
				fetchProducts()
				closeEdit()
			}
		})
	}

	function disableProduct (productId) {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			if (response) {
				Swal.fire({
					title: "Disable Product",
					icon: "success",
					text: "Item Disabled!"
				})
				fetchProducts()
				setProductId("");
				closeDisable()
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something Went Wrong"
				})
				fetchProducts()
				setProductId("");
				closeDisable()
			}
		})
	}

	function enableProduct (productId) {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/${productId}/unarchive`,{
			method: "PUT",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response =>  response.json()).then(response => {
				if (response) {
					Swal.fire({
						title: "Enable Product",
						icon: "success",
						text: "Item Enabled!"
					})
					fetchProducts()
					setProductId("");
					closeEnable()
				} else {
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Something went wrong."
					})
					fetchProducts()
					setProductId("");
					closeEnable();
				}
			})
	}

	function deleteProduct (productId) {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/${productId}/delete-product`, {
			method: "DELETE",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			if (response) {
				Swal.fire({
					title: "Delete Product",
					icon: "success",
					text: "Item Deleted"
				})
				fetchProducts()
				setProductId("");
				closeDelete()
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
				fetchProducts()
				setProductId("");
				closeDelete();
			}
		})
	}


	useEffect(() => {

		function addToCart(productId) {
			//console.log(`working`)

			// find the cart first for the user then if a product is found on the user return a message saying that the product is already added

			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/retrieve-cart`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
			}).then(response => response.json()).then(response => {
				const { orderedProducts } = response
				
				let productFinder = orderedProducts.find((product) =>{ return product.productId === productId})

				if (productFinder !== undefined) {
					Swal.fire({
						title: "Item Already Added to Cart",
						icon: "error",
						text: "Please check cart."
					})
				} else {
					fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/${productId}/add-to-cart`, {
						method: "POST",
						headers: {
							"Content-Type" : "application/json",
							"Authorization" : `Bearer ${localStorage.getItem("token")}`
						}
					}).then(response => response.json()).then(response => {
						if (response) {
							Swal.fire({
								title: "Add to Cart",
								icon: "success",
								text: "Item successfully added to cart!"
							})
						} else {
							Swal.fire({
								title: "Error",
								icon: "error",
								text: "Something went wrong"
							})
						}
					})
				}	
			})	
		}

		const productList = productProperty.map((product, index) => {
			const { _id, productName, productDescription, productPrice, isActive} = product

			return (
			<Fragment>
				<Card className="m-4" style={{ width: '12rem' }}>
				  <Card.Body>
				    <Card.Title className="text-center">{productName}</Card.Title>
				    <Card.Text>{productDescription}</Card.Text>
				   <Card.Title>PHP {productPrice}</Card.Title>
				   
				   <div className="text-center p-2">
				   	{
				   		(isActive) ?
				   		<Button variant="light" size="sm" 
				   				className="font-weight-bold btn btn-outline-dark"
				   				onClick={() => {
				   					addToCart(_id)
				   				}}>Add to Cart</Button>
				   	:
				   	null
				   	}
				   	{
				   		(isActive) ?
				   		<Button variant="light" 
				   				size="sm"
				   				className="font-weight-bold btn-outline-dark mt-2"
				   				onClick={() => {
				   					setProductId(_id)
				   					openDisable();
				   				}}>Disable Product</Button>
				   	:
				   		<Button variant="light" size="sm" 
				   				className="font-weight-bold btn-outline-dark mt-2"
				   				onClick={() => {
				   					setProductId(_id);
				   					openEnable();
				   				}}>Enable Product</Button>
				   	}
				   		<Button variant="light" 
				   				size="sm" 
				   				className="font-weight-bold btn-outline-dark mt-2"
				   				onClick={() => {
				   					setProductId(_id)
				   					openDelete()
				   				}}>Delete Product</Button>
				   		<Button variant="light" 
				   				size="sm"
				   				className="font-weight-bold btn-outline-dark mt-2"
				   				onClick={() => {
				   					setPreviousName(productName);
				   					setPreviousDescription(productDescription);
				   					setPreviousPrice(productPrice);
				   					setProductId(_id);
				   					openEdit()
				   				}}>Update Product</Button>			
				   	</div>
				  </Card.Body>
				</Card>
			</Fragment>
			)
		})

		setProductCard(productList)

	},[productProperty])
	
	
	return (
		<Fragment>
			{productCard}

				{/*Disable Product Modal*/}
				<Modal show={showDisable} onHide={closeDisable}>
					<Modal.Header>
						<Modal.Title>Disable Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Label>Do you want to disable this product?</Form.Label>
							<Button variant="light" 
									className="btn btn-outline-dark m-3" 
									onClick={() => {
										disableProduct(productID)
									}}>Yes</Button>
							<Button variant="light" className="btn btn-outline-dark m-3" onClick={closeDisable}>No</Button>
							</Form>
						</Modal.Body>
				</Modal>

				{/*Enable Product Modal*/}
				<Modal show={showEnable} onHide={closeEnable}>
					<Modal.Header>
						<Modal.Title>Enable Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Label>Do you want to enable this product?</Form.Label>
							<Button variant="light" 
									className="btn btn-outline-dark m-3" 
									onClick={() => {
										enableProduct(productID)
									}}>Yes</Button>
							<Button variant="light" className="btn btn-outline-dark m-3" onClick={closeEnable}>No</Button>
							</Form>
						</Modal.Body>
				</Modal>

				{/*Enable Product Modal*/}
				<Modal show={showDelete} onHide={closeDelete}>
					<Modal.Header>
						<Modal.Title>Delete Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Label>Do you want to delete this product?</Form.Label>
							<Button variant="light" 
									className="btn btn-outline-dark m-3" 
									onClick={() => {
										deleteProduct(productID)
									}}>Yes</Button>
							<Button variant="light" className="btn btn-outline-dark m-3" onClick={closeDelete}>No</Button>
							</Form>
						</Modal.Body>
				</Modal>

				{/*Update Product Modal*/}
				<Modal show={showEdit} onHide={closeEdit}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Group controlId="courseName">
								<Form.Label>Product Name:</Form.Label>
								<Form.Control 	type="text"
												value={name}
												placeholder={previousName}
												onChange={(event) => {
													setName(event.target.value);
												}}/>
							</Form.Group>
							<Form.Group controlId="courseDescription">
								<Form.Label>Description:</Form.Label>
								<Form.Control 	type="text"
												value={description}
												placeholder={previousDescription}
												onChange={(event) => {
													setDescription(event.target.value);
												}}/>
							</Form.Group>
							<Form.Group controlId="price">
								<Form.Label>Price:</Form.Label>
								<Form.Control 	type="number"
												value={price}
												placeholder={previousPrice}
												onChange={(event) => {
													setPrice(event.target.value);
												}}/>
							</Form.Group>
							<Button variant="light" 
									className="btn btn-outline-dark m-3" 
									onClick={() => {
										updateProduct(productID)
									}}>Submit</Button>
							<Button variant="light" className="btn btn-outline-dark m-3" onClick={closeEdit}>Close</Button>
							</Form>
						</Modal.Body>
				</Modal>
			</Fragment>
	)

}
