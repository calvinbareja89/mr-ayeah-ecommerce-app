//Module
import { Fragment, useState } from 'react';
import { Carousel } from 'react-bootstrap';

import model1 from './../assets/model1.jpg';
import model2 from './../assets/model2.jpg';
import model3 from './../assets/model3.jpg'

export default function AppHeader () {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
  	<Fragment>
  			<header class="bg-dark py-5">
  			    <div class="container px-4 px-lg-5 my-5">
  			        <div class="text-center text-white">
  			            <h1 class="display-4 fw-bolder">aYeah's Men Apparel 2021</h1>
  			            <p class="lead fw-normal text-white-50 mb-0">Check out our latest products offered.</p>
  			        </div>
  			    </div>
  			</header>
  		  <Carousel activeIndex={index} onSelect={handleSelect}>
  		    <Carousel.Item>
  		      <img
  		        className="d-block w-100"
  		        src={model1}
  		        alt="First slide"
  		      />
  		      <Carousel.Caption>
  		        <h3>Get 10% Discount for newly registered users</h3>
  		        <p>Available upon check out.</p>
  		      </Carousel.Caption>
  		    </Carousel.Item>
  		    <Carousel.Item>
  		      <img
  		        className="d-block w-100"
  		        src={model2}
  		        alt="Second slide"
  		      />

  		      <Carousel.Caption>
  		        <h3>Deliver in any location.</h3>
  		        <p>International delivery is also available to listed countries.</p>
  		      </Carousel.Caption>
  		    </Carousel.Item>
  		    <Carousel.Item>
  		      <img
  		        className="d-block w-100"
  		        src={model3}
  		        alt="Third slide"
  		      />

  		      <Carousel.Caption>
  		        <h3>Get amazing perks when you become a loyalty member.</h3>
  		        <p>Its easy as 1-2-3.</p>
  		      </Carousel.Caption>
  		    </Carousel.Item>
  		  </Carousel>
  	</Fragment>
  );
}

