import { useState, useEffect, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { Card, Button} from 'react-bootstrap'; 

export default function ProductCardGuest (props) {

	const { productProperty } = props
	const [productCard, setProductCard] = useState([]);

	useEffect(() => {

		const productList = productProperty.map((product) => {
			const { productName, productDescription, productPrice} = product

			return (
			<Fragment>
				<Card className="m-4" style={{ width: '12rem' }}>
				  <Card.Body>
				    <Card.Title className="text-center">{productName}</Card.Title>
				    <Card.Text>{productDescription}</Card.Text>
				   <Card.Title>PHP {productPrice}</Card.Title>
				   
				   <div className="text-center p-2">
		   			<Button variant="light" size="sm" 
		   				className="font-weight-bold btn btn-outline-dark"
		   				as={NavLink}
		   				to="/register"
		   				>Register Now!</Button>
		  		 	</div>
				  </Card.Body>
				</Card>	
			</Fragment>
					)
			}) 
		setProductCard(productList)

	}, [productProperty])

	return (
		<Fragment>
			{
				productCard
			}
		</Fragment>
	)
}